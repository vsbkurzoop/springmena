# Zadání domácího úkolu bloku JAVA pro pokročilé #

Vytvořte webovou aplikaci pomocí frameworku Spring, která bude poskytovat připojeným uživatelům aktuální kurzy světových měn. 

Aktuální kurzy bude aplikace získávat přes API společnosti ExchangeRateLab - http://www.exchangeratelab.com

Výstupem bude webový stránka s tabulkou aktuálních směnných kurzů nacházející se na adrese localhost:port/kurzy

# Úkol2 #

Každá měna bude mít svou webovou stránku na adrese localhost:port/<nazev_meny> s výpisem aktuálního kurzu.

